# Yii2 Project Template Basic

Repositori ini ditujukan untuk membuat aplikasi menjadi lebih mudah. 


#### Resep :

- Yii 2.0.34
- AdminLTE 3.0
- GiiAnt 0.11

#### Cara Instalasi :

- Lakukan `composer install`.
- Lakukan `yii migrate`.
- Buka sesuai path di komputermu semisal : `http://localhost/yii2-project-template-basic/web`