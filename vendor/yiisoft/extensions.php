<?php

$vendorDir = dirname(__DIR__);

return array (
  'pheme/yii2-toggle-column' => 
  array (
    'name' => 'pheme/yii2-toggle-column',
    'version' => '0.8.0.0',
    'alias' => 
    array (
      '@pheme/grid' => $vendorDir . '/pheme/yii2-toggle-column',
    ),
  ),
  'pheme/yii2-settings' => 
  array (
    'name' => 'pheme/yii2-settings',
    'version' => '0.7.0.0',
    'alias' => 
    array (
      '@pheme/settings' => $vendorDir . '/pheme/yii2-settings',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.1.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'dmstr/yii2-helpers' => 
  array (
    'name' => 'dmstr/yii2-helpers',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@dmstr/helpers' => $vendorDir . '/dmstr/yii2-helpers/src',
    ),
  ),
  'dmstr/yii2-db' => 
  array (
    'name' => 'dmstr/yii2-db',
    'version' => '3.0.1.0',
    'alias' => 
    array (
      '@dmstr/db' => $vendorDir . '/dmstr/yii2-db/db',
      '@dmstr/console' => $vendorDir . '/dmstr/yii2-db/console',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.11.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'dmstr/yii2-bootstrap' => 
  array (
    'name' => 'dmstr/yii2-bootstrap',
    'version' => '0.2.2.0',
    'alias' => 
    array (
      '@dmstr/bootstrap' => $vendorDir . '/dmstr/yii2-bootstrap',
    ),
  ),
  'schmunk42/yii2-giiant' => 
  array (
    'name' => 'schmunk42/yii2-giiant',
    'version' => '0.11.2.0',
    'alias' => 
    array (
      '@schmunk42/giiant' => $vendorDir . '/schmunk42/yii2-giiant/src',
    ),
    'bootstrap' => 'schmunk42\\giiant\\Bootstrap',
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker/src',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.1.3.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.1.22.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
);
